import {Component} from "react";
import ToyotaButton from "../components/ToyotaButton";
import React from "react";


class ThirdComponent extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        year: ''
    };

    checked = (checked, id) => {
        if (checked) {
            this.setState({year: id});
        }
        this.props.data && this.props.data(id);
    };

    render() {

        return (
            <div className="component temiz-containe third-component three-buttons">
                <div className={"container"}>
                    <div className={"row"}>
                        <div className="col-sm-2"></div>
                        <div className="col-sm-8">
                            <h3>Hybrid avantajını kaç yıllık kullanım üzerinden görmek istersiniz?
                            </h3>
                            <p>Aracınızı kaç yıl kullanacağınızı aşağıdaki seçeneklerden seçiniz.</p>
                            <ToyotaButton onClick={this.checked} id={"3"}
                                          isChecked={this.state.year === "3"}
                                          className={"btn btn-toyota"}>
                                3 Yıl
                            </ToyotaButton>
                            <ToyotaButton className={"btn btn-toyota"} onClick={this.checked} id={"4"}
                                          isChecked={this.state.year === "4"}>
                                4 Yıl
                            </ToyotaButton>
                            <ToyotaButton className={"btn btn-toyota"} onClick={this.checked} id={"5"}
                                          isChecked={this.state.year === "5"}>
                                5 Yıl
                            </ToyotaButton>
                            <br/>
                            <h3 className={"second-component-second-header"}>Toyota Hybrid,</h3>
                            <p>maliyet avantajının yanında,benzinli ve dizel araçlara göre çok dah düşük CO2 emisyon değerleriyle çevreyi korumaya katkı sağlıyor. Hibrit araç kullanarak sadece 3 yılda, benzinli bir araca göre ortalama 1.7 ton CO2 emisyon tasarrufu elde edebilirsiniz.</p>
                        </div>
                        <div className="col-sm-2"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ThirdComponent;
