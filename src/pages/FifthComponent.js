import {Component} from "react";
import ToyotaButton from "../components/ToyotaButton";
import React from "react";


class FifthComponent extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        place: ''
    };

    checked = (checked, id) => {
        if (checked) {
            this.setState({place: id});
        }
        this.props.data && this.props.data(id);
    };

    render() {

        return (
            <div className="component temiz-containe fifth-component">
                <div className={"container"}>
                    <div className={"row"}>
                        <div className="col-sm-2"></div>
                        <div className="col-sm-8">
                            <h3>Aracınızı ağırlıklı olarak nerede kullanacaksınız?</h3>
                            <ToyotaButton onClick={this.checked} id={"sehirici"}
                                          isChecked={this.state.place === "sehirici" ? true : false}
                                          className={"btn btn-toyota"}>
                                Şehir İçi
                            </ToyotaButton>
                            <ToyotaButton className={"btn btn-toyota"} avarage={false} onClick={this.checked} id={"ortalama"}
                                          isChecked={this.state.place === "ortalama" ? true : false}>
                                Ortalama
                            </ToyotaButton>
                            <br/>
                            <h3 className={"second-component-second-header"}>Hybrid otomobillerde</h3>
                            <p>ortalama yakıt tüketimi, diğer motor seçeneklerine göre %56'ya varan oranlarda daha
                                düşüktür.</p>
                        </div>
                        <div className="col-sm-2"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FifthComponent;
