import React from "react";
import Logo from "../components/Logo";

export default () => {
    return (
        <div className="component first-component rps-scroll--disabled">
            <div className={"container"}>
                <div className={"row index-content"}>
                    <div className={"col-sm-2"}>
                    </div>
                    <div className={"col-sm-8"}>
                        <div className={"versus-container"}>
                            <span className={"versus"}>vs.</span>
                            <span className={"versus-detail"}>Dizel - Benzinli</span>
                        </div>
                        <div className={"versus-desc-container"}>
                            <span className={"versus-desc"}>Hem Güvenli Hem Avantajlı!</span>
                            <span className={"versus-desc-text"}>
                                <strong>Toyota Corolla Hybrid</strong>'in diğer motor seçeneklerine göre avantajlarını keşfetmetmeye hazır mısınız?
                            </span>
                        </div>
                    </div>
                    <div className={"col-sm-2"}></div>
                </div>
            </div>
        </div>
    )
}
