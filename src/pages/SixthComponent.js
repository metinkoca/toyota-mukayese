import {Component} from "react";
import ToyotaButton from "../components/ToyotaButton";
import React from "react";
import LoadingIndicatorStory from "../components/LoadingIndicatorStory";
import {formatMoney} from "../helper";


class SixthComponent extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        motorType: '',
        detailedReport: false,
        charSize: 200,
        width: 0,
        height: 0,
        showSticky: false,
        showTestDriveButton: false,
        localLogo: false,
        chartValue: 75,
        isMobile: false,
        chartWidth: 30,
        showPopup: false,
        yilSec: true
    };

    constructor(props) {
        super(props);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        var elmnt = document.getElementById("test");
        elmnt.addEventListener('scroll', this.updateSticky);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({width: window.innerWidth, height: window.innerHeight});
        if (window.innerWidth < 375) {
            this.setState({charSize: 160, chartWidth: 20, isMobile: true});
        } else if (window.innerWidth < 760) {
            this.setState({charSize: 190, chartWidth: 20, isMobile: true});
        } else {
            this.setState({charSize: 250, chartWidth: 30, isMobile: false});
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.isVisible) {
            setTimeout(() => {
                this.setState({localLogo: true});
            }, 0);
        }
        if (nextProps.calculations.deviceType === "benzinli") {
            this.setState({chartValue: 75});
        } else {
            this.setState({chartValue: 70});
        }
    }

    updateSticky = () => {
        var elmnt = document.getElementById("test").scrollTop;
        let offset = document.getElementById("sticky-starter").offsetTop;

        if (window.innerWidth < 760) {
            if (elmnt > 220) {
                this.setState({showSticky: true});
                this.props.hideLogo && this.props.hideLogo(true);
                if (elmnt > offset + 109) {
                    this.setState({showTestDriveButton: true});
                } else {
                    this.setState({showTestDriveButton: false});
                }
            } else {
                this.props.hideLogo && this.props.hideLogo(false);
                this.setState({showSticky: false});
            }
        } else {
            if (elmnt > offset + 39) {
                this.props.hideLogo && this.props.hideLogo(true);
                this.setState({showSticky: true});
                if (elmnt > offset + 109) {
                    this.setState({showTestDriveButton: true});
                } else {
                    this.setState({showTestDriveButton: false});
                }
            } else {
                this.props.hideLogo && this.props.hideLogo(false);
                this.setState({showSticky: false});
            }
        }
        //260

    };

    checked = (checked, id) => {
        this.props.data && this.props.data('benzinli');
    };

    checkedDizel = (checked, id) => {
        this.props.data && this.props.data('dizel');
    };


    checkedYear = (e) => {
        this.setState({year: e.target.value});
        this.props.year && this.props.year(e.target.value, false);
    };

    checkedKm = (e) => {
        this.setState({km: e.target.value});
        this.props.km && this.props.km(e.target.value, false);
    };


    checkedPlace = (e) => {
        this.setState({place: e.target.value});
        this.props.place && this.props.place(e.target.value, false);
    };


    /*
        yilSec = () => {
            return (<Secim show={this.state.yilSec} onClose={() => this.setState({yilSec: false})}>
                Lütfen yıl seçiniz:
                <select name="" id="secim" className={"form-control"} onChange={this.checkedYear}>
                    <option value="3" checked={this.props.calculations.year === "3" ? true : ""}>3</option>
                    <option value="4" checked={this.props.calculations.year === "4" ? true : ""}>4</option>
                    <option value="5" checked={this.props.calculations.year === "5" ? true : ""}>5</option>
                </select><br/><br/>
                Lütfen Kilometre seçiniz:
                <select name="" id="secim" className={"form-control"} onChange={this.checkedKm}>
                    <option value="10000">10000</option>
                    <option value="15000">15000</option>
                    <option value="20000">20000</option>
                </select>
            </Secim>);
        };
        */

    render() {
        return (
            <div className="component sixth-component" id={"test"}>
                <div className="static-logo show-on-mobile" style={this.state.localLogo ? {opacity: 1} : {opacity: 0}}>
                    <img
                        src={require('./../images/logo.png')} alt=""/></div>
                {this.state.showSticky ? <div className={"sticky-banner"}>
                    <div className={"container"}>
                        <div className={"row"}>
                            <div className={"col-sm-2"}></div>
                            <div className={"col-sm-9"}>
                                <div className="row">
                                    <div className={"col-sm-3 col-5"}>
                                        <img src={require('./../images/hybridLogo.png')}
                                             style={{marginTop: '10px', width: '100%'}}
                                             alt=""/>
                                    </div>
                                    <div className={"col-sm-4 col-7"}>
                                        <button className={"btn btn-result"} onClick={this.checked} id={"benzinli"}
                                                style={this.props.calculations.deviceType ? this.props.calculations.deviceType === "benzinli" ? {
                                                    background: '#ff0000',
                                                    color: '#fff'
                                                } : null : null}>BENZİNLİ
                                        </button>
                                        <button className={"btn btn-result"} onClick={this.checkedDizel} id={"dizel"}
                                                style={this.props.calculations.deviceType ? this.props.calculations.deviceType === "dizel" ? {
                                                    background: '#ff0000',
                                                    color: '#fff'
                                                } : null : null}>DİZEL
                                        </button>
                                    </div>
                                    <div className={"col-sm-1"}></div>
                                    <div className={"col-sm-4 hide-on-mobile"}>
                                        {!this.props.isBayi ? <button className={"make-test-drive"} onClick={() => {
                                            window.location.href = "https://turkiye.toyota.com.tr/basvuru/YeniToyota.aspx";
                                        }}
                                                                      style={this.state.showTestDriveButton ? {display: 'block'} : {display: 'block'}}>
                                            <img src={require('./../images/whell.png')}
                                                 className={"make-test-drive-img"} alt=""/>
                                            <img src={require('./../images/ico-pager-white.png')}
                                                 className={"make-test-drive-ico"} alt=""/>
                                            <span>Test Sürüşü Yapın</span>
                                        </button> : null}
                                        {this.props.isBayi ? <button className={"detailed-report"}
                                                                     style={!this.state.showTestDriveButton ? {display: 'block'} : {display: 'block'}}
                                                                     onClick={() => {
                                                                         this.setState({detailedReport: !this.state.detailedReport});
                                                                         setTimeout(function () {
                                                                             var elmnt = document.getElementById("details");
                                                                             elmnt.scrollIntoView();
                                                                         }, 1);
                                                                     }}>
                                            <span>Detaylı Rapor</span>
                                            <span className={"detailed-report-icon"}><img
                                                src={require('./../images/ico-pager-white.png')} alt=""/></span>
                                        </button> : null}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : null}
                <div className={"container page"} style={{paddingTop: '10px'}}>
                    <div className={"row"}>
                        <div className="col-sm-2"></div>
                        <div className="col-sm-9">
                            <h3>Toplam Araca Sahip Olma Maliyeti Karşılaştırması</h3>
                            {this.props.calculations.deviceType === "benzinli" ?
                                (<p>Corolla Hybrid Flame X-Pack modelinin, benzinli Corolla Flame X-Pack modeliyle ortalama 2.el değerleri, bakım maliyetleri, yakıt tüketimleri, vergileri gibi değerlerinin karşılaştırması sonucudur.</p>) : (<p>Corolla Hybrid Flame X-Pack modelinin, C segmentindeki dizel modellerin ortalama
                                    tahmini 2.el değerleri, bakım maliyetleri, yakıt tüketimleri, vergileri gibi
                                    değerlerinin karşılaştırması sonucudur.</p>)}
                            <div className={"row"} id={"sticky-starter"}>
                                <div className={"col-sm-3 col-5"}>
                                    <img src={require('./../images/hybridLogo.png')}
                                         style={{marginTop: '10px', width: '100%'}}
                                         alt=""/>
                                </div>
                                <div className={"col-sm-4 col-7"}>
                                    <button className={"btn btn-result"} onClick={this.checked} id={"benzinli"}
                                            style={this.props.calculations.deviceType ? this.props.calculations.deviceType === "benzinli" ? {
                                                background: '#ff0000',
                                                color: '#fff'
                                            } : null : null}>BENZİNLİ
                                    </button>
                                    <button className={"btn btn-result"} onClick={this.checkedDizel} id={"dizel"}
                                            style={this.props.calculations.deviceType ? this.props.calculations.deviceType === "dizel" ? {
                                                background: '#ff0000',
                                                color: '#fff'
                                            } : null : null}>DİZEL
                                    </button>
                                </div>
                                <div className={"col-sm-1"}></div>
                                {this.props.isBayi ? <div className={"col-sm-4 hide-on-mobile"}>
                                    <button className={"detailed-report"}
                                            onClick={() => {
                                                this.setState({detailedReport: !this.state.detailedReport});
                                                if (!this.state.detailedReport) {
                                                    setTimeout(function () {
                                                        var elmnt = document.getElementById("details");
                                                        elmnt.scrollIntoView();
                                                    }, 1);
                                                }
                                            }}>
                                        <span>Detaylı Rapor</span>
                                        <span className={"detailed-report-icon"}><img
                                            src={require('./../images/ico-pager-white.png')} alt=""/></span>
                                    </button>
                                </div> : null}
                            </div>
                            <div className={"details"}>
                                <div className={"row"}>
                                    <div className={"col-sm-12 col-12"}>
                                        <div className={"row"}>
                                            <div className={"col-sm-4 col-6"}>
                                                <LoadingIndicatorStory
                                                    value={this.props.calculations.deviceType === "benzinli" ? this.props.calculations.percentageBenzin : this.props.calculations.percentageDizel}
                                                    size={this.state.charSize}
                                                    strokewidth={this.state.chartWidth}
                                                    valuelabel={this.props.calculations.deviceType === "benzinli" ? this.props.calculations.percentageBenzin : this.props.calculations.percentageDizel}
                                                    valuelabelMin={"%"}/>
                                                {!this.state.isMobile ? <div className={"car-content"}>
                                                    <span className={"car-name"}>Corolla Hybrid</span>
                                                    <span className={"car-details"}>Toplam Sahip Olma<br/>Maliyeti Avantajı</span>
                                                    <span
                                                        className={"buying-price-included"}>*Satın alma fiyatı dahil</span>
                                                </div> : null}
                                            </div>
                                            <div className={"col-sm-8 col-6"}>
                                                {this.state.isMobile ? <div className={"car-content"}>
                                                    <span className={"car-name"}>Corolla Hybrid</span>
                                                    <span className={"car-details"}>Toplam Sahip Olma<br/>Maliyeti Avantajı</span>
                                                    <span
                                                        className={"buying-price-included"}>*Satın alma fiyatı dahil</span>
                                                </div> : null}
                                                <div className={"did-try hide-on-mobile"}>
                                                    <b>Yeni Corolla Hybrid</b> ile test<br/>sürüşüne çıkmaya hazır
                                                    mısın?
                                                    <div className={"did-try-call"} onClick={() => {
                                                        window.location.href = "https://turkiye.toyota.com.tr/basvuru/YeniToyota.aspx";
                                                    }}>
                                                        <div className={"row"}>
                                                            <div className={"col-sm-2 col-2"}
                                                                 style={{paddingRight: '5px'}}>
                                                                <img src={require('./../images/ico-chart.png')}
                                                                     width={"100%"}
                                                                     alt=""/>
                                                            </div>
                                                            <div className={"col-sm-9 col-9"}>
                                                                <span><b>Test Sürüşü Yapmak İstiyorum</b><br/>Sizi Arayalım</span>
                                                                <img src={require('./../images/ico-pager-red.png')}
                                                                     className={"call-icon"} style={{filter: ''}}
                                                                     alt=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!this.state.isMobile ? <div className={"car-detailed-desc"}>
                                                    Mevcut Corolla, C SD segmentindeki bazı muadili
                                                    <br/><b>otomobillerin ortalaması ile karşılaştırıldığında,
                                                    seçtiğiniz
                                                    değerlerde, dizel otomobillere göre
                                                    aşagidaki oranda daha fazla maliyet avantajı sağlıyor!</b>
                                                    <br/>
                                                    {this.props.isBayi ? <button onClick={() => {
                                                            this.setState({detailedReport: !this.state.detailedReport});
                                                            if (!this.state.detailedReport) {
                                                                setTimeout(function () {
                                                                    var elmnt = document.getElementById("details");
                                                                    elmnt.scrollIntoView();
                                                                }, 1);
                                                            }
                                                        }}
                                                                                 className={this.state.detailedReport ? "detailed-report-car-desc active" : "detailed-report-car-desc"}>Detaylı
                                                            Rapor
                                                        </button> :
                                                        <p className={"customer-info"}>*Detayları öğrenmek için
                                                            bayilerimizi ziyaret edin! </p>}
                                                    <div id={"details"}></div>
                                                    <p className={"legal-information"}
                                                       onClick={() => this.props.showPopup && this.props.showPopup()}>
                                                        <b>*Yasal
                                                            bilgilendirme
                                                            ;</b> hybrid,
                                                        benzinli ve dizel karşılaştırması</p>
                                                </div> : null}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {this.props.isBayi ? <div className={"car-price"}>
                                <h3>Anahtar Teslim Fiyat</h3>
                                <table className="table">
                                    <tbody>
                                    <tr>
                                        <td className={"line-box first-td"}>
                                            <div className="d-flex">
                                                <span className="mr-2"><img src={require('./../images/car.png')}
                                                                            alt="" style={{
                                                    width: '100px',
                                                    marginTop: '15px'
                                                }}/></span>
                                                <span><p>COROLLA HYBRID<br/>
FLAME X PACK</p><br/><p>{formatMoney(this.props.calculations.hybidPrice)}</p></span></div>
                                        </td>
                                        {this.props.calculations.deviceType === "benzinli" ?
                                            <td className={"line-box second-td"}>
                                                <span><p>COROLLA 1.6L MDS<br/>FLAME X PACK</p><br/><p>{formatMoney(this.props.calculations.benzinPrice)}</p></span>
                                            </td> : null}

                                        {this.props.calculations.deviceType !== "benzinli" ?
                                            <td className={"line-box second-td"}>
                                                <span><p>C SD SEGMENTİ ORTALAMA<br/>TAHMİNİ DİZEL ARAÇ<br/><br/></p><p>{formatMoney(this.props.calculations.dizelPrice)}</p></span>
                                            </td> : null}
                                        <td className={"third-td"}>
                                            <span
                                                className={"special-header"}>{this.props.calculations.deviceType === "benzinli" ? "BENZİNLİ'YE" : "DİZEL'E"} GÖRE HYBRİD FARKI</span>
                                            <span><p
                                                style={this.props.calculations.deviceType ? this.props.calculations.deviceType === "benzinli" ? {color: '#ff0000'} : null : null}>{this.props.calculations.deviceType === "benzinli" ? formatMoney(-(this.props.calculations.hybidPrice - this.props.calculations.benzinPrice)) : formatMoney(-(this.props.calculations.hybidPrice - this.props.calculations.dizelPrice))}</p></span>
                                        </td>
                                    </tr>

                                    </tbody>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div> : null}
                            {this.props.isBayi ? <button className={"detailed-report show-on-mobile"}
                                                         onClick={() => this.setState({detailedReport: !this.state.detailedReport})}>
                                <span>Detaylı Rapor</span>
                                <span className={"detailed-report-icon"}><img
                                    src={require('./../images/ico-pager-white.png')} alt=""/></span>
                            </button> : null}
                        </div>
                    </div>
                </div>
                {this.state.detailedReport ? <div>
                    <div className={"detailed-section"}>
                        <div className={"container"}>
                            <div className={"row"}>
                                <div className={"col-sm-2"}></div>
                                <div className={"col-sm-9"}>
                                    <h3>Değer Kaybı</h3>
                                    <table className={"table"}>
                                        <tr>
                                            <td className={"line-box first-td"}>
                                                <div className="d-flex justify-content-between">
                                                    <span>2.EL</span><br/>
                                                    <span>{this.props.calculations.ikinciElDegerKayipYuzdesiHybid} %</span>
                                                </div>
                                                <div className="d-flex justify-content-between">
                                                    <span>{this.props.calculations.year} yıl/{formatMoney(this.props.calculations.toplamkm)} KM</span><br/>
                                                    <span>{formatMoney(this.props.calculations.ikinciElKayipFiyatiHybrid)}</span>
                                                </div>
                                            </td>

                                            {this.props.calculations.deviceType === "benzinli" ?
                                                <td className={"line-box second-td"}>
                                                    <span>{this.props.calculations.ikinciElDegerKayipYuzdesiBenzin} %</span><br/>
                                                    <span
                                                        className="mobile-color">{formatMoney(this.props.calculations.ikinciElKayipFiyatiBenzin)}</span>
                                                </td> : null}

                                            {this.props.calculations.deviceType !== "benzinli" ?
                                                <td className={"line-box second-td"}>
                                                    <span>{parseInt(this.props.calculations.ikinciElDegerKayipYuzdesiDizel)}%</span><br/>
                                                    <span>{formatMoney(this.props.calculations.ikinciElKayipFiyatiDizel)}</span>
                                                </td> : null}
                                            <td className={"third-td"}>
                                                <div className={"advantage"}>Avantajınız!</div>
                                                <span
                                                    style={this.props.calculations.deviceType === "benzinli" ? this.props.calculations.degerkaybiBenzin < 0 ? {color: '#ff0000'} : null : this.props.calculations.degerkaybiDizel < 0 ? {color: '#ff0000'} : null}>{this.props.calculations.deviceType === "benzinli" ? formatMoney(this.props.calculations.degerkaybiBenzin) : formatMoney(this.props.calculations.degerkaybiDizel)}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={"detailed-section"}>
                        <div className={"container"}>
                            <div className={"row"}>
                                <div className={"col-sm-2"}></div>
                                <div className={"col-sm-9"}>
                                    <h3>Yıllık Vergiler ve Bakım Maliyeti</h3>
                                    <table className={"table"}>
                                        <tr>
                                            <td className={"line-box first-td"}>
                                                <div className="d-flex justify-content-between">
                                                    <span>Bakım Maliyeti</span><br/>
                                                    <span>{formatMoney(this.props.calculations.bakimHybid)}</span>
                                                </div>
                                                <div className="d-flex justify-content-between">
                                                    <span>MTV(yıllık)</span><br/>
                                                    <span>{formatMoney(this.props.calculations.mtvHybid)}</span>
                                                </div>
                                                <div className="d-flex justify-content-between">
                                                    <span>Yıl<YearSelect
                                                        year={this.checkedYear} calculations={this.props.calculations}/> </span><br/>
                                                    <span>{this.props.calculations.year}</span>

                                                </div>
                                                <div className="d-flex justify-content-between font-weight-bold">
                                                    <span>Toplam MTV & Vergiler</span><br/>
                                                    <span>{formatMoney(this.props.calculations.toplamBakimVergiHybrid)}</span>
                                                </div>
                                            </td>
                                            {this.props.calculations.deviceType === "benzinli" ?
                                                <td className={"line-box second-td"}>
                                                    <span>{formatMoney(this.props.calculations.bakimBenzin)}</span><br/>
                                                    <span>{formatMoney(this.props.calculations.mtvBenzin)}</span><br/>
                                                    <span>{this.props.calculations.year}</span><br/>
                                                    <span
                                                        className="font-weight-bold">{formatMoney(this.props.calculations.toplamBakimVergiBenzin)}</span>
                                                </td> : null}
                                            {this.props.calculations.deviceType !== "benzinli" ?
                                                <td className={"line-box second-td"}>
                                                    <span>{formatMoney(this.props.calculations.bakimDizel)}</span><br/>
                                                    <span>{formatMoney(this.props.calculations.mtvDizel)}</span><br/>
                                                    <span>{this.props.calculations.year}</span><br/>
                                                    <span
                                                        className="font-weight-bold">{formatMoney(this.props.calculations.toplamBakimVergiDizel)}</span>
                                                </td> : null}
                                            <td className={"third-td"}>
                                                <span
                                                    style={this.props.calculations.deviceType === "benzinli" ? this.props.calculations.bakimVergiBenzin < 0 ? {color: '#ff0000'} : null : this.props.calculations.bakimVergiDizel < 0 ? {color: '#ff0000'} : null}>{this.props.calculations.deviceType === "benzinli" ? formatMoney(this.props.calculations.bakimVergiBenzin) : formatMoney(this.props.calculations.bakimVergiDizel)}</span>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={"detailed-section"}>
                        <div className={"container"}>
                            <div className={"row"}>
                                <div className={"col-sm-2"}></div>
                                <div className={"col-sm-9"}>
                                    <h3>Yakıt Maliyeti</h3>
                                    <table className={"table"}>
                                        <tr>
                                            <td className={"line-box first-td"}>
                                                <div className="d-flex justify-content-between">
                                                    <span>Yakıt Tüketimi / {this.props.calculations.place === "ortalama" ? "Ortalama" : "Şehiriçi"}
                                                        <PlaceSelect
                                                            place={this.checkedPlace}
                                                            calculations={this.props.calculations}/></span><br/>
                                                    <span>{this.props.calculations.yakitHybrid}</span>
                                                </div>
                                                <div className="d-flex justify-content-between">
                                                    <span>1 LT/TL</span><br/>
                                                    <span>{this.props.calculations.litreHybrid}</span>
                                                </div>
                                                <div className="d-flex justify-content-between font-weight-bold">
                                                    <span>{this.props.calculations.year} Yıllık KM <KmSelect
                                                        km={this.checkedKm}
                                                        calculations={this.props.calculations}/> </span><br/>
                                                    <span>{formatMoney(this.props.calculations.year * this.props.calculations.km)}</span>
                                                </div>
                                                <div className="d-flex justify-content-between font-weight-bold">
                                                    <span>Toplam Yakıt</span><br/>
                                                    <span>{formatMoney(this.props.calculations.toplamYakitHybrid)}</span>
                                                </div>
                                            </td>
                                            {this.props.calculations.deviceType === "benzinli" ?
                                                <td className={"line-box second-td"}>
                                                    <span>{this.props.calculations.yakitBenzin}</span><br/>
                                                    <span>{this.props.calculations.litreBenzin}</span><br/>
                                                    <span>{formatMoney(this.props.calculations.year * this.props.calculations.km)}</span><br/>
                                                    <span>{formatMoney(this.props.calculations.toplamYakitBenzin)}</span>

                                                </td> : null}
                                            {this.props.calculations.deviceType !== "benzinli" ?
                                                <td className={"line-box second-td"}>
                                                    <span>{this.props.calculations.yakitDizel}</span><br/>
                                                    <span>{this.props.calculations.litreDizel}</span><br/>
                                                    <span>{formatMoney(this.props.calculations.year * this.props.calculations.km)}</span><br/>
                                                    <span>{formatMoney(this.props.calculations.toplamYakitDizel)}</span>
                                                </td> : null}
                                            <td className={"third-td"}>
                                                <div className={"advantage"}>Avantajınız!</div>
                                                <span>{this.props.calculations.deviceType === "benzinli" ? formatMoney(this.props.calculations.toplamYakitHesapBenzin) : formatMoney(this.props.calculations.toplamYakitHesapDizel)}</span>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={"detailed-section"}>
                        <div className={"container"}>
                            <div className={"row"}>
                                <div className={"col-sm-2"}></div>
                                <div className={"col-sm-9"}>
                                    <h3>Egzos Emisyonu</h3>
                                    <table className={"table"}>
                                        <tr>
                                            <td className={"line-box first-td"}>
                                                <div className="d-flex justify-content-between">
                                                    <span>Egzos emisyon (3 yıl sonra)</span><br/>
                                                    <span>0</span>
                                                </div>
                                            </td>

                                            {this.props.calculations.deviceType === "benzinli" ?
                                                <td className={"line-box second-td"}>
                                                    <span>65</span>
                                                </td> : null}
                                            {this.props.calculations.deviceType !== "benzinli" ?
                                                <td className={"line-box second-td"}>
                                                    <span>65</span>
                                                </td> : null}
                                            <td className={"third-td"}>
                                                <div className={"advantage"}>Avantajınız!</div>
                                                <span>65</span>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : null}
                <div className={"did-try show-on-mobile"} onClick={() => {
                    window.location.href = "https://turkiye.toyota.com.tr/basvuru/YeniToyota.aspx";
                }}>
                    <b>Yeni Corolla Hybrid</b> ile test<br className={"hide-on-mobile"}/>sürüşüne çıkmaya hazır mısın?
                    <div className={"did-try-call"}>
                        <div className={"row"}>
                            <div className={"col-sm-2 col-2"} style={{paddingRight: '5px'}}>
                                <img src={require('./../images/ico-chart.png')} width={"100%"}
                                     alt=""/>
                            </div>
                            <div className={"col-sm-9 col-9"}>
                                <span><b>Test Sürüşü Yapmak İstiyorum</b><br/>Sizi Arayalım</span>
                                <img src={require('./../images/ico-pager-red.png')}
                                     className={"call-icon"} style={{filter: ''}} alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.isMobile ? <div className={"car-detailed-desc"}>
                    Mevcut Corolla, C SD segmentindeki bazı muadili
                    <br className={"hide-on-mobile"}/><b>otomobillerin ortalaması ile karşılaştırıldığında,
                    seçtiğiniz
                    değerlerde, dizel otomobillere göre
                    aşagidaki oranda daha fazla maliyet avantajı sağlıyor!</b>
                    <br/>
                    {this.props.isBayi ? null :
                        <p className={"customer-info"}>*Detayları öğrenmek için
                            bayilerimizi ziyaret edin! </p>}
                    <div id={"details"}></div>
                    <p className={"legal-information"} onClick={() => this.props.showPopup && this.props.showPopup()}>
                        <b>*Yasal
                            bilgilendirme
                            ;</b> hybrid,
                        benzinli ve dizel karşılaştırması</p>
                </div> : null}
            </div>
        );
    }
}

class YearSelect extends Component {
    state = {show: false};
    checkedYear = (e) => {
        this.setState({year: e.target.value, show: false});
        this.props.year && this.props.year(e, false);
    };

    render() {
        if (this.props.calculations.changeable) {
            return (<div style={{display: 'inline-block'}}>
                {!this.state.show ?
                    <button className={"edit-btn"} onClick={() => this.setState({show: true})}><img
                        src={require('./../images/writing.svg')} style={{height: '20px'}}/></button> : null}
                {this.state.show ?
                    <select name="" className={"form-control select-options"} onChange={this.checkedYear}>
                        <option value="3" selected={this.props.calculations.year === "3" ? true : ""}>3</option>
                        <option value="4" selected={this.props.calculations.year === "4" ? true : ""}>4</option>
                        <option value="5" selected={this.props.calculations.year === "5" ? true : ""}>5</option>
                    </select> : null}
            </div>);
        } else {
            return null;
        }
    }
}

class KmSelect extends Component {
    state = {show: false};

    checkedKm = (e) => {
        this.setState({km: e.target.value, show: false});
        this.props.km && this.props.km(e, false);
    };

    render() {
        if (this.props.calculations.changeable) {
            return (<div style={{display: 'inline-block'}}>
                {!this.state.show ?
                    <button className={"edit-btn"} onClick={() => this.setState({show: true})}><img
                        src={require('./../images/writing.svg')} style={{height: '20px'}}/></button> : null}
                {this.state.show ? <select name="" id="secim" className={"form-control"} onChange={this.checkedKm}>
                    <option value="10000" selected={this.props.calculations.km === "10000" ? true : ""}>10000 / Yıl
                    </option>
                    <option value="15000" selected={this.props.calculations.km === "15000" ? true : ""}>15000 / Yıl
                    </option>
                    <option value="20000" selected={this.props.calculations.km === "20000" ? true : ""}>20000 / Yıl
                    </option>
                </select> : null}
            </div>);
        } else {
            return null;
        }
    }
}

class PlaceSelect extends Component {
    state = {show: false};

    checkedPlace = (e) => {
        this.setState({km: e.target.value, show: false});
        this.props.place && this.props.place(e, false);
    };

    render() {
        if (this.props.calculations.changeable) {
            return (<div style={{display: 'inline-block'}}>
                {!this.state.show ? <button className={"edit-btn"} onClick={() => this.setState({show: true})}><img
                    src={require('./../images/writing.svg')} style={{height: '20px'}}/></button> : null}
                {this.state.show ? <select name="" id="secim" className={"form-control"} onChange={this.checkedPlace}>
                    <option value="ortalama"
                            selected={this.props.calculations.place === "ortalama" ? true : ""}>Ortalama
                    </option>
                    <option value="sehirici"
                            selected={this.props.calculations.place !== "ortalama" ? true : ""}>Şehiriçi
                    </option>
                </select> : null}
            </div>);
        } else {
            return null;
        }
    }
}

export default SixthComponent;
