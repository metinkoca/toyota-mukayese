import React, {Component} from 'react';
import ToyotaButton from "../components/ToyotaButton";


class SecondComponent extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        motorType: ''
    };

    checked = (checked, id) => {
        if (checked) {
            this.setState({motorType: id});
        }
        this.props.data && this.props.data(id);
    };

    render() {

        return (
            <div className="component temiz-containe second-component">
                <div className={"container"}>
                    <div className={"row"}>
                        <div className="col-sm-2"></div>
                        <div className="col-sm-8">
                            <h3>Hybrid avantajını</h3>
                            <p>Hybrid maliyet avantajını karşılaştırmak istediğiniz motor seçeneğini seçiniz:</p>
                            <ToyotaButton onClick={this.checked} id={"benzinli"}
                                          isChecked={this.state.motorType === "benzinli" ? true : false}
                                          className={"btn btn-toyota"}>
                                Benzinli
                            </ToyotaButton>
                            <ToyotaButton className={"btn btn-toyota"} onClick={this.checked} id={"dizel"}
                                          isChecked={this.state.motorType === "dizel" ? true : false}>
                                Dizel
                            </ToyotaButton>
                            <br/>
                            <h3 className={"second-component-second-header"}>Hybrid ile hem konforlu,</h3>
                            <p>hem de diğer motor seçeneklerine göre maliyet olarak çok daha avantajlı. Satın aldıktan sonra da bakım maliyetleri, yakıt tüketim, 2.el değeri gibi konularda diğer motor seçeneklerine göre çok daha avantajlı!</p>
                        </div>
                        <div className="col-sm-2"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SecondComponent;
