import {Component} from "react";
import ToyotaButton from "../components/ToyotaButton";
import React from "react";

class FourthComponent extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        km: ''
    };

    checked = (checked, id) => {
        if (checked) {
            this.setState({km: id});
        }
        this.props.data && this.props.data(id);
    };

    render() {

        return (<div className="component temiz-containe fourth-component three-buttons">
            <div className={"container"}>
                <div className={"row"}>
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <h3>Aracınızı yılda ortalama kaç kilometre kullanırsınız?</h3>
                        <ToyotaButton onClick={this.checked} id={"10000"}
                                      isChecked={this.state.km === "10000"}
                                      className={"btn btn-toyota"}>
                            10.000 Km
                        </ToyotaButton>
                        <ToyotaButton className={"btn btn-toyota"} onClick={this.checked} id={"15000"}
                                      isChecked={this.state.km === "15000"}>
                            15.000 Km
                        </ToyotaButton>
                        <ToyotaButton className={"btn btn-toyota"} onClick={this.checked} id={"20000"}
                                      isChecked={this.state.km === "20000"}>
                            20.000 Km
                        </ToyotaButton>
                        <br/>
                        <h3 className={"second-component-second-header"}>Hybrid araçlarda</h3>
                        <p>yaklaşık balata değişimleri 60bin km'de, diğer araçlarda ise 30bin km'de değişmektedir. Hybrid rejeneretif frenleme sayesinde frenler, yaklaşık 2 katı daha fazla ömürlü olarak kullanılabilmektedir. Üstelik Hybrid otomobillerde triger kayışı da bulunmuyor; bu avantajların tümü Hybridlerin bakım maliyetlerinin diğer motor seçeneklerine göre daha düşük olmasını sağlıyor!</p>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        </div>);
    }
}

export default FourthComponent;

