import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

class ToyotaButton extends PureComponent {
    click = () => {
        this.props.onClick && this.props.onClick(!this.props.isChecked, this.props.id);
    };

    render() {
        return (
            <button onClick={this.click}
                    className={this.props.isChecked ? this.props.avarage ? "btn btn-toyota btn-toyota-checked btn-toyo-spec" : "btn btn-toyota btn-toyota-checked" : this.props.avarage ? "btn btn-toyota btn-toyo-spec" : "btn btn-toyota"}>
                {this.props.isChecked ? <div className={"tick"}>
                    <img src={require('./../images/tick.svg')} style={{width: '20px'}} alt=""/>
                </div> : null}
                {this.props.children}
            </button>
        );
    }
}

ToyotaButton.propTypes = {
    isChecked: PropTypes.bool
};

export default ToyotaButton;
