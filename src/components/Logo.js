import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

class Logo extends PureComponent {
    render() {
        return (<div className={"container"}>
                <div className={"row"}>
                    <div className={"col-sm-2"}>
                        <div className={"logo"}>
                            <img src={require('./../images/logo.png')} alt=""/>
                        </div>
                    </div>
                    <div className={"col-sm-8"}></div>
                    <div className={"col-sm-2"}></div>
                </div>
            </div>
        );
    }
}

Logo.propTypes = {};

export default Logo;
