import React, {Component} from 'react';
import PropTypes from 'prop-types';

class PageNums extends Component {
    static defaultProps = {
        activeStep: 0
    };

    static propTypes = {
        activeStep: PropTypes.number
    };

    state = {
        containerClass: 'page-nums'
    };

    componentWillReceiveProps(nextProps, nextContext) {
        switch (nextProps.activeStep) {
            case 5:
                this.setState({containerClass: 'page-nums black-nums hide-on-mobile'});
                break;
            case 4:
                this.setState({containerClass: 'page-nums white-nums'});
                break;
            case 3:
                this.setState({containerClass: 'page-nums white-nums'});
                break;
            case 2:
                this.setState({containerClass: 'page-nums white-nums'});
                break;
            case 1:
                this.setState({containerClass: 'page-nums white-nums'});
                break;
            default:
                this.setState({containerClass: 'page-nums'});
                break;
        }
    }

    goBack = (number) => {
        if (this.props.activeStep !== 5) {
            this.props.goToPage(number);
        }
    };

    renderMove = () => {
        switch (this.props.activeStep) {
            case 6:
                return (
                    <button className={"operation mobile-move-container"}
                            style={{color: '#fff', fontSize: '25px', opacity: 1}}>
                        <span className={"mobile-move"}>Devam</span><br/>
                        <div className={"clearfix"}></div>
                        <img src={require('./../images/ico-pager-white.png')} alt=""/>
                    </button>);
                break;
            case 5:
                return (
                    <button className={"operation mobile-move-container hide-on-mobile"}
                            onClick={() => this.props.goToPage(6)}
                            style={{color: '#fff', fontSize: '25px', opacity: 1}}>
                        <span className={"mobile-move"}>Devam</span><br/>
                        <div className={"clearfix"}></div>
                        <img src={require('./../images/ico-pager-white.png')} alt=""/>
                    </button>);
                break;
            case 4:
                return (
                    <button className={"operation mobile-move-container"} onClick={() => this.props.goToPage(5)}
                            style={{color: '#fff', fontSize: '25px', opacity: 1}}>
                        <span className={"mobile-move"}>Devam</span><br/>
                        <div className={"clearfix"}></div>
                        <img src={require('./../images/ico-pager-white.png')} alt=""/>
                    </button>);
                break;
            case 3:
                return (
                    <button className={"operation mobile-move-container"} onClick={() => this.props.goToPage(4)}
                            style={{color: '#fff', fontSize: '25px', opacity: 1}}>
                        <span className={"mobile-move"}>Devam</span><br/>
                        <div className={"clearfix"}></div>
                        <img src={require('./../images/ico-pager-white.png')} alt=""/>
                    </button>);
                break;
            case 2:
                return (
                    <button className={"operation mobile-move-container"} onClick={() => this.props.goToPage(3)}
                            style={{color: '#fff', fontSize: '25px', opacity: 1}}>
                        <span className={"mobile-move"}>Devam</span><br/>
                        <div className={"clearfix"}></div>
                        <img src={require('./../images/ico-pager-white.png')} alt=""/>
                    </button>);
                break;
            case 1:
                return (
                    <button className={"operation mobile-move-container"} onClick={() => this.props.goToPage(2)}
                            style={{color: '#fff', fontSize: '25px', opacity: 1}}>
                        <span className={"mobile-move"}>Devam</span><br/>
                        <div className={"clearfix"}></div>
                        <img src={require('./../images/ico-pager-white.png')} alt=""/>
                    </button>);
                break;
            default:
                return (
                    <button className={"operation"} onClick={() => this.props.goToPage(1)}>
                        O zaman<br className={"hide-on-mobile"}/><span className={"mobile"}> başlayalım</span>
                        <div className={"clearfix"}></div>
                        <img src={require('./../images/ico-pager.png')} alt=""/>
                    </button>);
                break;
        }
    };

    render() {
        return (
            <div className={this.state.containerClass}>
                <ul>
                    <li  onClick={()=>this.goBack(1)} style={{cursor:'pointer'}} className={this.props.activeStep === 1 ? "active" : null}>01</li>
                    <li onClick={()=>this.goBack(2)} style={{cursor:'pointer'}} className={this.props.activeStep === 2 ? "active" : null}>02</li>
                    <li onClick={()=>this.goBack(3)} style={{cursor:'pointer'}} className={this.props.activeStep === 3 ? "active" : null}>03</li>
                    <li onClick={()=>this.goBack(4)} style={{cursor:'pointer'}} className={this.props.activeStep === 4 ? "active" : null}>04</li>
                    <li className={this.props.activeStep === 5 ? "active" : null}>05</li>
                </ul>
                <div className={"clearfix"}></div>
                {this.renderMove()}
            </div>
        );
    }

}

export default PageNums;
