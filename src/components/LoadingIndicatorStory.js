import React, {Component} from 'react';
import PropTypes from 'prop-types';

class LoadingIndicatorStory extends Component {
    static propTypes = {
        value: PropTypes.number,        // value the chart should show
        valuelabel: PropTypes.string,   // label for the chart
        size: PropTypes.number,         // diameter of chart
        strokewidth: PropTypes.number   // width of chart line
    };

    state = {
        value: 0
    };

    static defaultProps = {
        value: 0,
        valuelabel: 'Completed',
        size: 180,
        strokewidth: 30
    };

    componentDidMount() {
        const a = setInterval(() => {
            if (this.state.value === this.props.value) {
                clearInterval(a);
            } else {
                this.setState({value: this.state.value + 1});
            }
        }, 25);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (this.state.value !== nextProps.value) {
            this.setState({value: 0});
            setTimeout(()=>{
                const a = setInterval(() => {
                    if (this.state.value === nextProps.value) {
                        clearInterval(a);
                    } else {
                        this.setState({value: this.state.value + 1});
                    }
                }, 25);
            },100);
        }
    }

    render() {

        const halfsize = (this.props.size * 0.5);
        const radius = halfsize - (this.props.strokewidth * 0.5);
        const circumference = 2 * Math.PI * radius;
        const strokeval = ((this.state.value * circumference) / 100);
        const dashval = (strokeval + ' ' + circumference);

        const trackstyle = {strokeWidth: this.props.strokewidth};
        const indicatorstyle = {strokeWidth: this.props.strokewidth, strokeDasharray: dashval}
        const rotateval = 'rotate(-90 ' + halfsize + ',' + halfsize + ')';

        return (
            <div style={{width: this.props.size, height: this.props.size, position: 'relative'}}>
                <svg width={this.props.size} height={this.props.size} className="donutchart">
                    <circle r={radius} cx={halfsize} cy={halfsize} transform={rotateval} style={trackstyle}
                            className="donutchart-track"/>
                    <circle r={radius} cx={halfsize} cy={halfsize} transform={rotateval} style={indicatorstyle}
                            className="donutchart-indicator"/>
                    <text className="donutchart-text" x={halfsize} y={halfsize + 7.5} style={{textAnchor: 'middle'}}>
                    </text>
                </svg>
                <div className={"chartLabel"}>{this.props.valuelabel} <span
                    className={"chartLabelMin"}>{this.props.valuelabelMin}</span></div>
            </div>
        );
    }
}

export default LoadingIndicatorStory;
