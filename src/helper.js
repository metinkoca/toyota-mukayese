function formatMoney(number, decPlaces, decSep, thouSep) {
    number = parseInt(number);
    if (typeof number == 'number') {
        return new Intl.NumberFormat({maximumSignificantDigits: 0}).format(number.toFixed(0));
    } else {
        return number;
    }
}

export {
    formatMoney
}
