import React, {Component} from 'react';
import PropTypes from 'prop-types';
import FirstComponent from "./pages/FirstComponent";
import SecondComponent from "./pages/SecondComponent";
import ThirdComponent from "./pages/ThirdComponent";
import FourthComponent from "./pages/FourthComponent";
import FifthComponent from "./pages/FifthComponent";
import ReactPageScroller from './components/ReactPageScroller';
import './App.css';
import Logo from "./components/Logo";
import PageNums from "./components/PageNums";
import SixthComponent from "./pages/SixthComponent";


const mtv = {
    dizel: {
        3: 6509,
        4: 8167,
        5: 9825
    },
    hybrid: {
        3: 8733,
        4: 11009,
        5: 13285
    },
    benzin: {
        3: 5397,
        4: 6746,
        5: 8095
    }
};

const bakim = {
    dizel: {
        10000: 362.1,
        30000: 2529.2,
        40000: 3677.9,
        45000: 3677.9,
        50000: 4711.7,
        60000: 6155.7,
        75000: 6921.9,
        80000: 8165.4,
        100000: 10919.9
    },
    hybrid: {
        10000: 0,
        30000: 1398.91,
        40000: 1398.91,
        45000: 2195.39,
        50000: 2195.39,
        60000: 3005.31,
        75000: 3704.77,
        80000: 3704.77,
        100000: 4475.20
    },
    benzin: {
        10000: 726.08,
        30000: 2134.69,
        40000: 3047.63,
        45000: 3047.63,
        50000: 3773.72,
        60000: 4774.24,
        75000: 5200.33,
        80000: 6488.39,
        100000: 8252.87
    }
};

const degerKaybi = {
    dizel: {
        3: {
            30000: 61.2,
            45000: 59.6,
            60000: 58.0
        },
        4: {
            40000: 49.9,
            60000: 48.8,
            80000: 47.9
        },
        5: {
            50000: 40.2,
            75000: 39.3,
            100000: 38.6
        }
    },
    hybrid: {
        3: {
            30000: 63.7,
            45000: 62.5,
            60000: 61.2
        },
        4: {
            40000: 54.2,
            60000: 53.2,
            80000: 52
        },
        5: {
            50000: 45.1,
            75000: 44.2,
            100000: 43.4
        }
    },
    benzin: {
        3: {
            30000: 61,
            45000: 59.6,
            60000: 58.5
        },
        4: {
            40000: 49.9,
            60000: 48.8,
            80000: 47.9
        },
        5: {
            50000: 40.2,
            75000: 39.3,
            100000: 38.6
        }
    }
};

const yakit = {
    dizel: {
        ortalama: 3.9,
        sehirici: 4.5
    },
    hybrid: {
        ortalama: 3.6,
        sehirici: 3.4
    },
    benzin: {
        ortalama: 5.6,
        sehirici: 7.3
    }
};

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentPage: 1,
            activeStep: 0,
            hybidPrice: 184000,
            benzinPrice: 164650,
            dizelPrice: 199867,
            shownDiff: -19350,
            ikinciElDegerKayipYuzdesiHybid: 0,
            ikinciElDegerKayipYuzdesiBenzin: 0,
            ikinciElDegerKayipYuzdesiDizel: 0,
            year: 3,
            km: 15000,
            mtvHybid: mtv.hybrid[3],
            mtvBenzin: mtv.benzin[3],
            mtvDizel: mtv.dizel[3],
            bakimHybid: 3739,
            bakimBenzin: 5415,
            bakimDizel: 5385,
            balataHybid: 644,
            balataBenzin: 1288,
            balataDizel: 1222,
            yakitHybid: 3.6,
            yakitBenzin: 5.6,
            yakitDizel: 3.8,
            yakitHybid1TL: 6.75,
            yakitBenzin1TL: 6.75,
            yakitDizel1TL: 6.31,
            emisyonHybid: 0,
            emisyonBenzin: 65,
            emisyonDizel: 65,
            deviceType: 'benzinli',
            isDeviceType: false,
            isYear: false,
            isKm: false,
            isPlace: false,
            removeLogo: false,
            isMobile: false,
            isBayi: false,
            fakeLoader: false,
            showPopup: false,
            changeable: false
        };
        this._pageScroller = null;

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);

        var urlParams = new URLSearchParams(window.location.search);
        const urlPath = window.location.pathname.split('/');
        if (urlParams.get('bayi') === 1 || urlParams.get('bayi') === "1") {
            this.setState({isBayi: true});
        }

        if (urlParams.has('deviceType')) {
            this.setState({deviceType: urlParams.get('deviceType')});
        }

        if (urlParams.has('year')) {
            this.setState({year: parseInt(urlParams.get('year'))});


        }

        if (urlParams.has('km')) {
            this.setState({km: parseInt(urlParams.get('km'))});
        }

        if (urlParams.has('place')) {
            this.setState({place: urlParams.get('place')});
        }
        if (urlParams.has('changeable')) {
            this.setState({changeable: urlParams.get('changeable')});
        }

        if (urlParams.has('move')) {
            this.setState({fakeLoader: true});
            setTimeout(() => {
                this._pageScroller.goToPage(5);
                setTimeout(() => {
                    this._pageScroller.goToPage(5);
                    this.setState({fakeLoader: false});
                }, 1100);
            }, 0);
        }
        setTimeout(() => {
            setTimeout(() => {
                this.calculations();
            }, 0);
        }, 0);

    }

    calculations = () => {

        const toplamkm = this.state.year * this.state.km;


        this.setState({toplamkm: toplamkm});
        /*değer kaybı
        * toplamkm
        * yıl
        * ikinciElDegerKayipYuzdesiHybid
        * ikinciElKayipFiyatiHybrid
        * ikinciElDegerKayipYuzdesiBenzin
        * ikinciElKayipFiyatiBenzin
        * ikinciElDegerKayipYuzdesiDizel
        * ikinciElKayipFiyatiDizel
        * degerkaybiBenzin
        * degerkaybiDizel
        * */
        /**
         * Hybrid Değer kaybı
         */


        let ikinciElDegerKayipYuzdesiHybid = this.state.ikinciElDegerKayipYuzdesiHybid;
        if (this.state.year === 3 && this.state.km === 10000) {
            ikinciElDegerKayipYuzdesiHybid = 61.0326086956522;

        } else if (this.state.year === 3 && this.state.km === 15000) {
            ikinciElDegerKayipYuzdesiHybid = 59.5652173913044;
        } else if (this.state.year === 3 && this.state.km === 20000) {
            ikinciElDegerKayipYuzdesiHybid = 58.0434782608696;
        } else if (this.state.year === 4 && this.state.km === 10000) {
            ikinciElDegerKayipYuzdesiHybid = 49.945652173913;
        } else if (this.state.year === 4 && this.state.km === 15000) {
            ikinciElDegerKayipYuzdesiHybid = 48.804347826087;
        } else if (this.state.year === 4 && this.state.km === 20000) {
            ikinciElDegerKayipYuzdesiHybid = 47.8804347826087;
        } else if (this.state.year === 5 && this.state.km === 10000) {
            ikinciElDegerKayipYuzdesiHybid = 40.2173913043478;
        } else if (this.state.year === 5 && this.state.km === 15000) {
            ikinciElDegerKayipYuzdesiHybid = 39.3478260869565;
        } else if (this.state.year === 5 && this.state.km === 20000) {
            ikinciElDegerKayipYuzdesiHybid = 38.5869565217391;
        }
        this.setState({
            ikinciElDegerKayipYuzdesiHybid: Math.round(ikinciElDegerKayipYuzdesiHybid * 10) / 10,
            ikinciElKayipFiyatiHybrid: this.roundHundred((ikinciElDegerKayipYuzdesiHybid / 100) * this.state.hybidPrice)
        });

        let ikinciElDegerKayipYuzdesiBenzin = this.state.ikinciElDegerKayipYuzdesiBenzin;
        if (this.state.year === 3 && this.state.km === 10000) {
            ikinciElDegerKayipYuzdesiBenzin = 60.9778317643486;
        } else if (this.state.year === 3 && this.state.km === 15000) {
            ikinciElDegerKayipYuzdesiBenzin = 59.5809292438506;
        } else if (this.state.year === 3 && this.state.km === 20000) {
            ikinciElDegerKayipYuzdesiBenzin = 58.548436076526;
        } else if (this.state.year === 4 && this.state.km === 10000) {
            ikinciElDegerKayipYuzdesiBenzin = 52.1105375037959;
        } else if (this.state.year === 4 && this.state.km === 15000) {
            ikinciElDegerKayipYuzdesiBenzin = 51.3817187974491;
        } else if (this.state.year === 4 && this.state.km === 20000) {
            ikinciElDegerKayipYuzdesiBenzin = 50.3492256301245;
        } else if (this.state.year === 5 && this.state.km === 10000) {
            ikinciElDegerKayipYuzdesiBenzin = 45.2474946856969;
        } else if (this.state.year === 5 && this.state.km === 15000) {
            ikinciElDegerKayipYuzdesiBenzin = 44.5794108715457;
        } else if (this.state.year === 5 && this.state.km === 20000) {
            ikinciElDegerKayipYuzdesiBenzin = 43.6683874886122;
        }

        this.setState({
            ikinciElDegerKayipYuzdesiBenzin: Math.round(ikinciElDegerKayipYuzdesiBenzin * 10) / 10,
            ikinciElKayipFiyatiBenzin: ((ikinciElDegerKayipYuzdesiBenzin / 100) * this.state.benzinPrice).toFixed(0),
            degerkaybiBenzin: this.roundHundred(((ikinciElDegerKayipYuzdesiHybid / 100) * this.state.hybidPrice) - ((ikinciElDegerKayipYuzdesiBenzin / 100 * this.state.benzinPrice)))
        });


        let ikinciElDegerKayipYuzdesiDizel = this.state.ikinciElDegerKayipYuzdesiDizel;
        if (this.state.year === 3 && this.state.km === 10000) {
            ikinciElDegerKayipYuzdesiDizel = 63.6724521084215;
        } else if (this.state.year === 3 && this.state.km === 15000) {
            ikinciElDegerKayipYuzdesiDizel = 62.4682620486032;
        } else if (this.state.year === 3 && this.state.km === 20000) {
            ikinciElDegerKayipYuzdesiDizel = 61.1622102594178;
        } else if (this.state.year === 4 && this.state.km === 10000) {
            ikinciElDegerKayipYuzdesiDizel = 54.1978044118650;
        } else if (this.state.year === 4 && this.state.km === 15000) {
            ikinciElDegerKayipYuzdesiDizel = 53.2291204649778;
        } else if (this.state.year === 4 && this.state.km === 20000) {
            ikinciElDegerKayipYuzdesiDizel = 52.0048082351646;
        } else if (this.state.year === 5 && this.state.km === 10000) {
            ikinciElDegerKayipYuzdesiDizel = 45.1257314341022;
        } else if (this.state.year === 5 && this.state.km === 15000) {
            ikinciElDegerKayipYuzdesiDizel = 44.2286299802800;
        } else if (this.state.year === 5 && this.state.km === 20000) {
            ikinciElDegerKayipYuzdesiDizel = 43.3939153444565;
        }
        this.setState({
            ikinciElDegerKayipYuzdesiDizel: Math.round(ikinciElDegerKayipYuzdesiDizel * 10) / 10,
            ikinciElKayipFiyatiDizel: ((ikinciElDegerKayipYuzdesiDizel / 100) * this.state.dizelPrice).toFixed(0),
            degerkaybiDizel: this.roundHundred(((ikinciElDegerKayipYuzdesiHybid / 100) * this.state.hybidPrice) - ((ikinciElDegerKayipYuzdesiDizel / 100) * this.state.dizelPrice))
        });


        /**
         * Yıllık vergiler ve bakım maliyeti
         *  year
         *  bakimHybid
         *  mtvHybid
         *  toplamBakimVergiHybrid
         *  bakimBenzin
         *  mtvBenzin
         * toplamBakimVergiBenzin
         * bakimDizel
         * mtvDizel
         * toplamBakimVergiDizel
         * bakimVergiBenzin
         * bakimVergiDizel
         */

        let bakimHybid = this.state.bakimHybid;
        if (toplamkm === 10000) {
            bakimHybid = 0;
        } else if (toplamkm === 30000) {
            bakimHybid = 1398.91;
        } else if (toplamkm === 40000) {
            bakimHybid = 1398.91;
        } else if (toplamkm === 45000) {
            bakimHybid = 2195.39;
        } else if (toplamkm === 50000) {
            bakimHybid = 2195.39;
        } else if (toplamkm === 60000) {
            bakimHybid = 3005.31;
        } else if (toplamkm === 75000) {
            bakimHybid = 3704.77;
        } else if (toplamkm === 80000) {
            bakimHybid = 3704.77;
        } else if (toplamkm === 100000) {
            bakimHybid = 4475.20;
        }
        this.setState({bakimHybid: bakimHybid.toFixed(0)});

        let mtvHybid = this.state.mtvHybid;
        if (this.state.year === 3) {
            mtvHybid = 8733;
        } else if (this.state.year === 4) {
            mtvHybid = 11009;
        } else if (this.state.year === 5) {
            mtvHybid = 13285;
        }
        this.setState({
            mtvHybid: mtvHybid,
            toplamBakimVergiHybrid: (bakimHybid + mtvHybid).toFixed(0)
        });

        let bakimBenzin = this.state.bakimBenzin;
        if (toplamkm === 10000) {
            bakimBenzin = 726.08;
        } else if (toplamkm === 30000) {
            bakimBenzin = 2134.69;
        } else if (toplamkm === 40000) {
            bakimBenzin = 3047.63;
        } else if (toplamkm === 45000) {
            bakimBenzin = 3047.63;
        } else if (toplamkm === 50000) {
            bakimBenzin = 3773.72;
        } else if (toplamkm === 60000) {
            bakimBenzin = 4774.24;
        } else if (toplamkm === 75000) {
            bakimBenzin = 5200.33;
        } else if (toplamkm === 80000) {
            bakimBenzin = 6488.39;
        } else if (toplamkm === 100000) {
            bakimBenzin = 8252.87;
        }
        this.setState({bakimBenzin: bakimBenzin.toFixed(0)});

        let mtvBenzin = this.state.mtvBenzin;
        if (this.state.year === 3) {
            mtvBenzin = 5397;
        } else if (this.state.year === 4) {
            mtvBenzin = 6746;
        } else if (this.state.year === 5) {
            mtvBenzin = 8095;
        }
        this.setState({
            mtvBenzin: mtvBenzin,
            toplamBakimVergiBenzin: (bakimBenzin + mtvBenzin).toFixed(0),
            bakimVergiBenzin: this.roundHundred(-(bakimHybid + mtvHybid - bakimBenzin - mtvBenzin))
        });

        let bakimDizel = this.state.bakimDizel;
        if (toplamkm === 10000) {
            bakimDizel = 362.1;
        } else if (toplamkm === 30000) {
            bakimDizel = 2529.2;
        } else if (toplamkm === 40000) {
            bakimDizel = 3677.9;
        } else if (toplamkm === 45000) {
            bakimDizel = 3677.9;
        } else if (toplamkm === 50000) {
            bakimDizel = 4711.7;
        } else if (toplamkm === 60000) {
            bakimDizel = 6155.7;
        } else if (toplamkm === 75000) {
            bakimDizel = 6921.9;
        } else if (toplamkm === 80000) {
            bakimDizel = 8165.4;
        } else if (toplamkm === 100000) {
            bakimDizel = 10919.9;
        }
        this.setState({bakimDizel: bakimDizel.toFixed(0)});

        let mtvDizel = this.state.mtvDizel;
        if (this.state.year === 3) {
            mtvDizel = 6509;
        } else if (this.state.year === 4) {
            mtvDizel = 8167;
        } else if (this.state.year === 5) {
            mtvDizel = 9825;
        }
        this.setState({
            mtvDizel: mtvDizel,
            toplamBakimVergiDizel: (bakimDizel + mtvDizel).toFixed(0),
            bakimVergiDizel: this.roundHundred(-(bakimHybid + mtvHybid - bakimDizel - mtvDizel))
        });

        let yakitHybrid = this.state.yakitHybrid;
        if (this.state.place === "ortalama") {
            yakitHybrid = 3.6;
        } else {
            yakitHybrid = 3.4;
        }
        this.setState({
            yakitHybrid: yakitHybrid,
            litreHybrid: 6.86,
            toplamYakitHybrid: (yakitHybrid * 6.86 * toplamkm / 100).toFixed(0)
        });

        let yakitBenzin = this.state.yakitBenzin;
        if (this.state.place === "ortalama") {
            yakitBenzin = 5.6;
        } else {
            yakitBenzin = 7.3;
        }
        this.setState({
            yakitBenzin: yakitBenzin,
            litreBenzin: 6.86,
            toplamYakitBenzin: (yakitBenzin * 6.86 * toplamkm / 100).toFixed(0),
            toplamYakitHesapBenzin: Math.round(-(-(yakitBenzin * 6.86 * toplamkm / 100) + (yakitHybrid * 6.86 * toplamkm / 100)))
        });

        let yakitDizel = this.state.yakitDizel;
        if (this.state.place === "ortalama") {
            yakitDizel = 3.9;
        } else {
            yakitDizel = 4.5;
        }
        this.setState({
            yakitDizel: yakitDizel,
            litreDizel: 6.52,
            toplamYakitDizel: (yakitDizel * 6.52 * toplamkm / 100).toFixed(0),
            toplamYakitHesapDizel: Math.round(-(-(yakitDizel * 6.52 * toplamkm / 100) + (yakitHybrid * 6.86 * toplamkm / 100)))
        });

        const hybridTotal = Math.round(-this.state.hybidPrice + (this.state.hybidPrice * (ikinciElDegerKayipYuzdesiHybid / 100)) - this.state.year - (yakitHybrid * 6.86 * toplamkm / 100));
        const hybridTotalForBenzin = Math.round(-this.state.hybidPrice + (this.state.hybidPrice * (ikinciElDegerKayipYuzdesiHybid / 100)) - (bakimHybid + mtvHybid) - (yakitHybrid * 6.86 * toplamkm / 100));
        const benzinTotal = Math.round(-this.state.benzinPrice + (this.state.benzinPrice * (ikinciElDegerKayipYuzdesiBenzin / 100)) - this.state.year - (yakitBenzin * 6.86 * toplamkm / 100) - 65);
        const dizelTotal = Math.round(-this.state.dizelPrice + Math.round((ikinciElDegerKayipYuzdesiDizel / 100) * this.state.dizelPrice) - (bakimDizel + mtvDizel) - (yakitDizel * 6.52 * toplamkm / 100) - 65);
        this.setState({
            percentageBenzin: Math.round((-(hybridTotal - benzinTotal) / benzinTotal) * 100),
            percentageDizel: Math.round((-(hybridTotalForBenzin - dizelTotal) / dizelTotal) * 100)
        });
    };

    roundHundred(value) {
        return Math.round(value / 100) * 100
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({width: window.innerWidth, height: window.innerHeight});
        if (window.innerWidth < 760) {
            this.setState({isMobile: true});
        } else {
            this.setState({isMobile: false});
        }
    }

    goToPage = (eventKey) => {
        if (eventKey === 1) {
            this._pageScroller.goToPage(eventKey);
            return;
        }
        if (eventKey === 2) {
            if (this.state.isDeviceType) {
                this._pageScroller.goToPage(eventKey);
            } else {
                this.setState({error: 'Lütfen Hybrid avantajını karşılaştıracağınız motor seçeneğini seçiniz.'});
                return;
            }
            return;
        }
        if (eventKey === 3) {
            if (this.state.isYear) {
                this._pageScroller.goToPage(eventKey);
            } else {
                this.setState({error: 'Lütfen Aracınızını kaç yıl kullanacağınızı seçiniz.'});
                return;
            }
            return;

        }
        if (eventKey === 4) {
            if (this.state.isKm) {
                this._pageScroller.goToPage(eventKey);
                return;
            } else {
                this.setState({error: 'Lütfen Aracınızını yılda kaç KM kullanacağınızı seçiniz.'});
                return;
            }
        }
        if (eventKey === 5) {
            if (this.state.isPlace) {
                this._pageScroller.goToPage(eventKey);
                return;
            } else {
                this.setState({error: 'Lütfen Aracınızını ağırlıklı olarak nerede kullanacağınzı seçiniz.'});
                return;
            }
        }

    };

    pageOnChange = (number) => {
        this.setState({currentPage: number});
        this.setState({activeStep: number - 1});
    };

    getPagesNumbers = () => {

        const pageNumbers = [];


        return [...pageNumbers];
    };


    secondData = (deviceType, isGo = true) => {
        this.setState({deviceType: deviceType, isDeviceType: true});
        if (isGo) {
            this._pageScroller.goToPage(2);
        }
        setTimeout(() => {
            this.calculations();
        }, 0);
    };


    secondData2 = (deviceType) => {
        this.setState({deviceType: deviceType, isDeviceType: true});

    };

    thirdData = (year, isGo = true) => {
        this.setState({year: parseInt(year), isYear: true});
        if (isGo) {
            this._pageScroller.goToPage(3);
        }
        setTimeout(() => {
            this.calculations();
        }, 0);
    };

    fourthData = (km, isGo = true) => {
        this.setState({km: parseInt(km), isKm: true});

        if (isGo) {
            this._pageScroller.goToPage(4);
        }
        setTimeout(() => {
            this.calculations();

        }, 0);
    };

    fifthData = (place, isGo = true) => {
        this.setState({place: place, isPlace: true});
        if (isGo) {
            this._pageScroller.goToPage(5);
        }
        setTimeout(() => {
            this.setState({removeLogo: true});
        }, 1000);

        setTimeout(() => {
            this.calculations();
        }, 0);
    };


    render() {
        const pagesNumbers = this.getPagesNumbers();
        return <React.Fragment>
            <ReactPageScroller ref={c => this._pageScroller = c} pageOnChange={this.pageOnChange}>
                <FirstComponent goToPage={this.goToPage}/>
                <SecondComponent goToPage={this.goToPage} data={this.secondData}/>
                <ThirdComponent goToPage={this.goToPage} data={this.thirdData}/>
                <FourthComponent goToPage={this.goToPage} data={this.fourthData}/>
                <FifthComponent goToPage={this.goToPage} data={this.fifthData}/>
                <SixthComponent calculations={this.state} isVisible={this.state.removeLogo} data={this.secondData2}
                                isBayi={this.state.isBayi} year={this.thirdData} km={this.fourthData}
                                place={this.fifthData}
                                showPopup={() => this.setState({showPopup: true, removeLogo: false})}
                                hideLogo={(a) => this.setState({removeLogo: a})}/>
            </ReactPageScroller>
            {this.state.error ? <div className={"error"}>{this.state.error}
                <div className={"closeIcon"} onClick={() => this.setState({error: ''})}><img
                    src={require("./images/delete.svg")} width={"20px"}
                    alt=""/></div>
            </div> : null}
            {this.state.error ? <div className={"overlay"} onClick={() => this.setState({error: ''})}></div> : null}
            <PageNums activeStep={this.state.activeStep} goToPage={this.goToPage}/>
            {this.state.removeLogo && this.state.isMobile ? null : <Logo/>}
            {this.state.fakeLoader ?
                <div className={"overlay-2"}>
                    <div className="lds-grid">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div> : null}

            {this.state.showPopup ?
                <div className={"legal-popup"}>
                    <div className={"legal-popup-text"}>
                        <button className={"cross-exit"}
                                onClick={() => this.setState({showPopup: false, removeLogo: true})}><img
                            src={require('./images/cross.png')} alt=""/>
                        </button>

                        {this.state.deviceType === "benzinli" ? <div>
                            <b>Benzinli karşılaştırması için:</b><br/><br/>

                            Bu oran, tahmini bir değer olup, farklı koşul ve zamanlarda değişiklik
                            gösterebilir.<br/>
                            Bu oran, Corolla Flame X-Pack versiyonunun satış fiyatı, bakım maliyeti, ikinci el
                            değeri, yakıt tüketimi gibi değerlerinin, C SD segmentindeki diğer markaların
                            modellerinin ortalamasıyla karşılaştırılması sonucunda çıkarılmıştır.<br/>
                            2.el değerleri ve bakım maliyetleri değerlerine, araştırma şirketleri vasıtasıyla,
                            gerekleştirilen araştırmalar sonucunda ulaşılmıştır.<br/>
                            Değerlendirme içerisinde yer alan otomobil fiyatları, 05.11.2019 tarihli, markaların
                            kendi websitelerinde yayınladıkları Tavsiye Edilen Fiyatlarıdır.
                        </div> : <div>
                            <b>Dizel karşılaştırmasi için:</b><br/><br/>

                            Bu oran, tahmini bir değer olup, farklı koşul ve zamanlarda değişiklik
                            gösterebilir.<br/>
                            Bu oran, Corolla Hybrid Flame X-Pack versiyonunun satış fiyatı, bakım maliyeti, ikinci
                            el
                            değeri, yakıt tüketimi gibi değerlerinin, C SD segmentindeki diğer markaların
                            modellerinin
                            ortalamasıyla karşılaştırılması sonucunda çıkarılmıştır.<br/>
                            2.el değerleri ve bakım maliyetleri değerlerine, araştırma şirketleri vasıtasıyla,
                            gerekleştirilen araştırmalar sonucunda ulaşılmıştır.<br/>
                            Değerlendirme içerisinde yer alan otomobil fiyatları, 05.11.2019 tarihli, markaların
                            kendi
                            websitelerinde yayınladıkları Tavsiye Edilen Fiyatlarıdır.
                        </div>}


                    </div>
                </div> : null}
        </React.Fragment>
    }
}

export default App;
